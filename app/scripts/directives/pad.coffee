@drumMachine.directive "pad", [ ->
  return {
    restrict: "EA"
    scope:
      sample: '='
      keys: "="
    template: "<div class='pad'>{{keys[0]}}</div>"
    link: (scope, element, attrs) ->
      sound = new Howl
        urls: [scope.sample]
        autoplay: false
      
      element.on "click", ->
        sound.play()
      
      Mousetrap.bind scope.keys, (e) ->
        sound.play()
        return false
  }
]