@drumMachine.controller "DrumMachineController", [ '$scope', '$location', ($scope, $location) ->
  $scope.testDrum = 'drums/kick.wav'
  $scope.keys = ['k', 'i', 'c', 'b']
  $scope.data =
    pads: [{
      sample: 'drums/kick.wav'
      keys: ['v','k']
    },
    {
      sample: "drums/Mini Hat.wav"
      keys: ['h', 'i'] 
    },
    {
      sample: "drums/Snare-17.wav"
      keys: ['s', 'n', 'b']
    }]
]