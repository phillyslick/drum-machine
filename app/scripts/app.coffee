'use strict'

@drumMachine = angular.module('drumMachineApp', ['ngCookies', 'ngResource', 'ngSanitize', 'ngRoute']
).config ['$routeProvider', ($routeProvider) ->
    $routeProvider
      .when '/',
        templateUrl: 'views/main.html'
        controller: 'MainCtrl'
      .when '/drum',
        templateUrl: "views/drum_machine.html"
        controller: "DrumMachineController"
      .otherwise
        redirectTo: '/'
  ]
